# Rick and Morty Challenge

![it defies all logic](morty.jpg)

# Executar o projeto:

```bash
docker compose up
```

[_Sobre o docker compose_](https://docs.docker.com/compose/)

Após inicar os processos, acesse [http://localhost:3002](http://localhost:3002) para ver a documentação da API.

# Testes

```bash
npm run test
```
